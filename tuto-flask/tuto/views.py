from .app import app, db
from .models import get_author, get_books_of_author, get_sample, get_book

from flask import render_template, url_for, redirect
from flask_wtf import FlaskForm
from wtforms import StringField, HiddenField
from wtforms.validators import DataRequired


class AuthorForm(FlaskForm):
    id = HiddenField('id')
    name = StringField('Nom', validators=[DataRequired()])


@app.route("/")
def home():
    return render_template(
        "home.html",
        title="Tiny Amazon",
        books=get_sample()
    )


@app.route("/book/<int:id>")
def book(id):
    book = get_book(id)
    return render_template(
        "book.html",
        title=book.title,
        book=book
    )


@app.route("/author/by-name/<string:name>")
def author_by_name(name):
    pass


@app.route("/author/<int:id>")
def author(id):
    author = get_author(id)
    return render_template(
        "author.html",
        title=author.name,
        author=author,
        books=get_books_of_author(id)
    )


@app.route("/edit/author/<int:id>")
def edit_author(id):
    a = get_author(id)
    f = AuthorForm(id=a.id, name=a.name)
    return render_template(
        "edit-author.html",
        author=a, form=f)


@app.route("/save/author/", methods=("POST",))
def save_author():
    a = None
    f = AuthorForm()
    if f.validate_on_submit():
        id = int(f.id.data)
        a = get_author(id)
        a.name = f.name.data
        db.session.commit()
        return redirect(url_for('author', id=a.id))
    a = get_author(int(f.id.data))
    return render_template(
        "edit-author.html",
        author=a, form=f)
